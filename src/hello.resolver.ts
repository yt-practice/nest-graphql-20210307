import { Query, ResolveField, Args, Resolver } from '@nestjs/graphql';
import { IQuery } from './graphql';

@Resolver('Hello')
export class HelloResolver implements IQuery {
  @Query()
  async Hello() {
    return { name: 'hoge', greet: '' };
  }
  @ResolveField('greet')
  async greet(@Args('name') name: string) {
    return `hello, ${name}!`;
  }
}
