
/** ------------------------------------------------------
 * THIS FILE WAS AUTOMATICALLY GENERATED (DO NOT MODIFY)
 * -------------------------------------------------------
 */

/* tslint:disable */
/* eslint-disable */
export interface IQuery {
    Hello(): Hello | Promise<Hello>;
}

export interface Hello {
    name: string;
    greet: string;
}
